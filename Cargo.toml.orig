[package]
name = "nettle-sys"
version = "2.3.1"
authors = [
    "Justus Winter <justus@sequoia-pgp.org>",
    "Kai Michaelis <kai@sequoia-pgp.org>",
]
edition = "2018"
description = "Low-level Rust bindings for the Nettle cryptographic library"
repository = "https://gitlab.com/sequoia-pgp/nettle-sys"
keywords = ["nettle", "cryptography"]
categories = ["external-ffi-bindings", "cryptography"]
license = "LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only"
readme = "README.md"
documentation = "https://docs.rs/nettle-sys/"
links = "nettle"

[dependencies]
libc = "0.2"

[build-dependencies]
bindgen = { version = ">= 0.62.0, < 0.71.0", default-features = false, features = ["runtime"] }
cc = "1"
pkg-config = "0.3"
tempfile = "3"

[target.'cfg(target_env = "msvc")'.build-dependencies]
vcpkg = "0.2.9"

[package.metadata.docs.rs]
dependencies = [ "pkg-config", "nettle-dev" ]
